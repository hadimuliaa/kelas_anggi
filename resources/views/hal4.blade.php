@extends('halamanku')
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">Starter</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                                    <li class="breadcrumb-item active">Starter</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->


                <div class="row">
                    <div class="col-xxl-12 col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <form method="POST" action="{{ route('save') }}">
                                        @csrf
                                        <label for="basiInput" class="form-label">Nama Barang</label>
                                        <input type="text" class="form-control" id="basiInput" name="inputKalimat"> <label for="basiInput" class="form-label">Nama Suplier</label>
                                        <input type="text" class="form-control" id="basiInput" name="inputKalimat2">

                                        <!-- Base Buttons -->
                                        <button type="submit" name="submit"
                                            class="btn btn-primary waves-effect waves-light">Kirim</button>
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> © Velzon.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        Design & Develop by Themesbrand
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </div>
@endsection
