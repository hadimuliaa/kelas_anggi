<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lat1 extends Model
{
    protected $table = 'latihan1';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'nama',
        'nama2',
        'created_at',
        'updated_at',
    ];
}
