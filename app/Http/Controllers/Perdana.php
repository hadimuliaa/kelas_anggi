<?php

namespace App\Http\Controllers;

use App\Models\Lat1;
use Illuminate\Http\Request;

class Perdana extends Controller
{
    public function index(Request $request)
    {
        $t1 = 2;
        $t2 = 'coba';
        $t2 = 6;
        $a = 20;
        $t = 16;

        $hasil = $t1;
        $hasil2 = $t1 * $t2;
        $luas = 1 / 2 * $a * $t;

        $hasil3 = ($t1 * 2) - $t2;
        // semua hukum ketentuan yang ada pda math itu berlaku juga pada pemograman
        // ====================================================== komentar pada php

        // LOGIKA IF
        // jika hasil bernilai 2 maka ok, jika tidak maka tidak ok

        if ($hasil == 2) {
            $hl = 'ok';
        } else {
            $hl = 'tidak ok';
        }

        if ($luas > 100) {
            $harga = 500;
        } else {
            $harga = 150;
        }

        // echo $hl;
        // echo "<br>";
        // echo "Alas segitiga = $a cm<br>";
        // echo "Tinggi segitiga = $t cm<br>";
        // echo "Luas segitiga = $luas cm<br>";
        // echo "Harga = $harga <br>";

        return view('hal1')->with([
            'data1' => $hl,
            'alas' => $a,
            'tinggi' => $t,
            'luas' => $luas,
            'harga' => $harga,
        ]);
    }
    public function duo(Request $request)
    {
        $t1 = 2;
        $t2 = 'coba';
        $t2 = 6;
        $a = 20;
        $t = 16;

        $hasil = $t1;
        $hasil2 = $t1 * $t2;
        $luas = 1 / 2 * $a * $t;

        $hasil3 = ($t1 * 2) - $t2;
        // semua hukum ketentuan yang ada pda math itu berlaku juga pada pemograman
        // ====================================================== komentar pada php

        // LOGIKA IF
        // jika hasil bernilai 2 maka ok, jika tidak maka tidak ok

        if ($hasil == 2) {
            $hl = 'ok';
        } else {
            $hl = 'tidak ok';
        }

        if ($luas > 100) {
            $harga = 500;
        } else {
            $harga = 150;
        }

        return view('hal2')->with([
            'data1' => 'iko datanyo',

        ]);
    }
    public function tigo(Request $request)
    {
        $t1 = 2;
        $t2 = 'coba';
        $t2 = 6;
        $a = 20;
        $t = 16;

        $hasil = $t1;
        $hasil2 = $t1 * $t2;
        $luas = 1 / 2 * $a * $t;

        $hasil3 = ($t1 * 2) - $t2;
        // semua hukum ketentuan yang ada pda math itu berlaku juga pada pemograman
        // ====================================================== komentar pada php

        // LOGIKA IF
        // jika hasil bernilai 2 maka ok, jika tidak maka tidak ok

        if ($hasil == 2) {
            $hl = 'ok';
        } else {
            $hl = 'tidak ok';
        }

        if ($luas > 100) {
            $harga = 500;
        } else {
            $harga = 150;
        }

        return view('hal3')->with([
            'data1' => 'iko datanyo',

        ]);
    }
    public function kirim(Request $request)
    {

        $dat = [
            'kal1' => $request->input('inputKalimat'),
            'kal2' => $request->input('inputKalimat2'),
        ];

        $kir = $dat ?? [];
        return view('hal3')->with([
            't' => $kir,

        ]);
    }
    public function inputlat(Request $request)
    {
        return view('hal4')->with([]);
    }
    public function save(Request $request)
    {
        $product = new Lat1(); // Buat instance dari model Product
        $product->nama = $request->input('inputKalimat');
        $product->nama2 = $request->input('inputKalimat2');
        // Update kolom lainnya jika diperlukan
        $product->save();
        return redirect(route('input'))->with('sukses', 'Data berhasil simpan.');
    }
    public function datJson(Request $request)
    {
        $ar = [
            'harga' => 50,
            'nama' => 'doni',
            'alamat' => 'afsdfds',
            'baru' => [
                'nama_barang' => 'coklat',
                'harga' => 90,
            ]
        ];
        $jsonData = json_encode($ar); // DALAM BENTUK JSON
        echo $jsonData;
        $dataArray = json_decode($jsonData, true); //INI BENTUK ARRAY

    }
}
