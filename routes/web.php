<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Perdana;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [Perdana::class, 'index'])->name('patamo');
Route::get('/kaduo', [Perdana::class, 'duo'])->name('kaduo');
Route::get('/katigo', [Perdana::class, 'tigo'])->name('katigo');
Route::post('/katigo', [Perdana::class, 'kirim'])->name('kirim');
//database
Route::get('/input', [Perdana::class, 'inputlat'])->name('input');
Route::post('/input', [Perdana::class, 'save'])->name('save');
